package com.ileossa.project.eucler;

import java.util.*;
import java.util.stream.Collectors;

public class Bootstrap {
        static final String EXEMPLE = "If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23 ";
        static final String QUESTION = "Find the sum of all the multiples of 3 or 5 below 1000";

    public static void main(String[] args){
        List<Integer> datas = initDatas(1000);
        System.out.printf("List de %d elements :%s \nResultat: %d", datas.size(), datas.toString(), resolve(datas, 3, 5));
    }

    public static long resolve(Collection<Integer> datas, int ... isMultiple){
        long res = 0;
        for (int nb: isMultiple) {
            res += sum(filterByValue(datas, nb));
        }
        return res;
    }


    /**
     * Permet de filtrer la liste et de garder uniquement les valeurs qui sont multiple de 'number'
     * @param datas
     * @param number
     * @return
     */
    public static List<Integer> filterByValue(Collection<Integer> datas, int number) {
        return datas.stream()
                .filter(data ->isMultiple(data, number))
                .collect(Collectors.toList());
    }


    /**
     * Permet de déterminer si la valeur est divisible par le multiple
     * @param value
     * @param multiple
     * @return
     */
    public static boolean isMultiple(int value, int multiple){
        if(value % multiple == 0){
            return true;
        }
        return false;
    }

    /**
     * Permet de faire la somme de tout les elements (Integer) dans la Collection passé
     * @param datas
     * @return
     */
    public static long sum(Collection<Integer> datas){
            return datas.stream()
                    .mapToInt(Integer::intValue)
                    .sum();
    }


    /**
     * Permet d'initialiser une list de n elements (-> numberOfValues)
     * @param numberOfValues
     * @return
     */
    public static List<Integer> initDatas(int numberOfValues){
        int value = 0;
        List<Integer> res = new ArrayList<>(numberOfValues);
        for(int i=0; i<= numberOfValues; i++){
            res.add(i);
        }
        return res;
    }





}
