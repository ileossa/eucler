package com.ileossa.project.eucler;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static com.ileossa.project.eucler.Bootstrap.*;


public class BootstrapTest {

    @Test
    public void should_init_10_data(){
        List<Integer> tenNumbers = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Assert.assertEquals(initDatas(10), tenNumbers);
    }

    @Test
    public void should_init_0_data(){
        List<Integer> tenNumbers = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Assert.assertEquals(initDatas(10), tenNumbers);
    }

    @Test
    public void should_sum_the_list(){
        Collection<Integer> datas = Arrays.asList(0, 1, 2);
        Assert.assertEquals(sum(datas), 3);
    }

    @Test
    public void should_sum_negative_numbers(){
        Collection<Integer> datas = Arrays.asList(1, 9, -10);
        Assert.assertEquals(sum(datas), 0);
    }

    @Test
    public void should_10_multiple_by_5(){
        Assert.assertEquals(isMultiple(10, 5), true);
    }

    @Test
    public void should_9_is_not_multiple_by_5(){
        Assert.assertEquals(isMultiple(9, 5), false);
    }

    @Test
    public void should_negative_three_is_not_multiple_by_10(){
        Assert.assertEquals(isMultiple(-3, 5), false);
    }

    @Test
    public void should_negative_20_is_multiple_by_2(){
        Assert.assertEquals(isMultiple(-20, 2), true);
    }

    @Test
    public void should_filter_the_list_values_by_multiple_five(){
        List<Integer> datas = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        List<Integer> finalResult = Arrays.asList(0, 5, 10);
        Assert.assertEquals(filterByValue(datas, 5), finalResult);
    }

    @Test
    public void should_filter_the_list_values_by_multiple_3(){
        List<Integer> datas = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        List<Integer> finalResult = Arrays.asList(0, 3, 6, 9);
        Assert.assertEquals(filterByValue(datas, 3), finalResult);
    }

    @Test
    public void should_return_resultat_23_with_10_datas_in_list(){
        List<Integer> datas = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        Assert.assertEquals(resolve(datas, 3, 5), 23);
    }

    @Test
    public void should_resolve_challenge_with_1000_datas_in_list_is_multiple_of_3_or_5_and_the_sum(){
        List<Integer> datas = initDatas(1000);
        Assert.assertEquals(resolve(datas, 3,5), 0);
    }

}